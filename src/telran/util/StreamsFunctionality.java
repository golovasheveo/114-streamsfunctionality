package telran.util;

import java.util.Random;

public class StreamsFunctionality {
    public static void displaySportLotoNumbers ( int min, int max, int nNumbers ) {

        if ( max - min + 1 < nNumbers )
            throw new RuntimeException ( "Numbers not found \n" );

        new Random ( )
                .ints ( min, max + 1 )
                .distinct ( )
                .limit ( nNumbers )
                .forEach ( n -> System.out.print ( n + "; " )
                );
    }

    public static void displayShufflingArray ( int[] ar ) {
        new Random ( ).ints ( 0, ar.length )
                .distinct ( )
                .limit ( ar.length )
                .forEach ( e -> System.out.print ( ar[ e ] + ", " ) );
    }


    public static void main ( String[] args ) {


        System.out.println ( "Loto numbers: " );
        displaySportLotoNumbers ( 0, 90, 91 );

        System.out.println ( "Shuffled array: " );
        displayShufflingArray ( new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 } );

    }
}